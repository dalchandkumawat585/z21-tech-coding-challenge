const express = require("express");
const axios = require("axios");
const app = express();
const port = process.env.PORT || 3000;

app.get("/", (req, res) => {
  res.send("Z21 Tech challenge is working!!!");
});

const months = [
  "Jan",
  "Feb",
  "Mar",
  "Apr",
  "May",
  "Jun",
  "Jul",
  "Aug",
  "Sep",
  "Oct",
  "Nov",
  "Dec",
];

const formatDate = (date) => {
  return (
    date.getDate() + "-" + months[date.getMonth()] + "-" + date.getFullYear()
  );
};

const getDates = () => {
  const day7 = formatDate(
    new Date(new Date().setDate(new Date().getDate() - 1))
  );
  const day1 = formatDate(
    new Date(new Date().setDate(new Date().getDate() - 7))
  );
  const day2 = formatDate(
    new Date(new Date().setDate(new Date().getDate() - 6))
  );
  const day3 = formatDate(
    new Date(new Date().setDate(new Date().getDate() - 5))
  );
  const day4 = formatDate(
    new Date(new Date().setDate(new Date().getDate() - 4))
  );
  const day5 = formatDate(
    new Date(new Date().setDate(new Date().getDate() - 3))
  );
  const day6 = formatDate(
    new Date(new Date().setDate(new Date().getDate() - 2))
  );
  return {
    day1,day2, day3, day4, day5, day6, day7
  };
};

const lastDayReturn = (obj, day) => {
  const returnValue =
    ((obj["day"+day] - obj["day1"]) / obj["day"+day]) * 100;
  obj["returnValue"] = returnValue;
  return returnValue;
};

const standardDeviation = (obj) => {
    if(obj["day1"]) {
        const return1 = obj["day2"] ? lastDayReturn(obj, 2): 0;
        const return2 = obj["day3"] ? lastDayReturn(obj, 3): 0;
        const return3 = obj["day4"] ? lastDayReturn(obj, 4): 0;
        const return4 = obj["day5"] ? lastDayReturn(obj, 5): 0;
        const return5 = obj["day6"] ? lastDayReturn(obj, 6): 0;
        const return6 = obj["day7"] ? lastDayReturn(obj, 7): 0;
        const average = (return1 + return2 + return3 + return4 + return5 + return6) / 6;
        const variance = (Math.pow(return1 - average, 2) + Math.pow(return2 - average, 2) + Math.pow(return3 - average, 2) + 
            Math.pow(return4 - average, 2) + Math.pow(return5 - average, 2) + Math.pow(return6 - average, 2)) / 6;
        const standardDeviation = Math.sqrt(variance);
        obj["standardDeviation"] = standardDeviation;
        return standardDeviation;
    }
}

const getDataFromApi = () => {  
  return new Promise((resolve, reject) => {
    const { day1, day2, day3, day4, day5, day6, day7 } = getDates();
    const url = `https://portal.amfiindia.com/DownloadNAVHistoryReport_Po.aspx?tp=2&frmdt=${day1}&todt=${day7}`;
    axios
      .get(url)
      .then((response) => {
        let data = response.data.split("\n");
        data.splice(0, 1);
        const actualData = {};
        for (let iterator = 0; iterator < data.length; iterator++) {
          if (data[iterator].includes(";")) {
            const row = data[iterator].split(";");
            const id = row[0];
            const name = row[1];
            const nav = row[4];
            const date = row[7].split("\r")[0];
            actualData[id] = { ...actualData[id], id, name, nav };
            switch(date) {
                case day1: actualData[id]["day1"] = nav; break;
                case day2: actualData[id]["day2"] = nav; break;
                case day3: actualData[id]["day3"] = nav; break;
                case day4: actualData[id]["day4"] = nav; break;
                case day5: actualData[id]["day5"] = nav; break;
                case day6: actualData[id]["day6"] = nav; break;
                case day7: actualData[id]["day7"] = nav; break;
            }
          }
        }
        const finalArray = [];
        Object.keys(actualData).forEach((key) => {
          if (actualData[key]["day1"] && actualData[key]["day7"])
            finalArray.push(actualData[key]);
        });
        resolve(finalArray);
      })
      .catch((err) => {
        console.log(err);
        reject();
      });
  });
};

app.get("/topTenOnReturns", (req, res) => {
  getDataFromApi()
    .then((finalArray) => {
      finalArray.sort((a, b) => {
        const returnsA = lastDayReturn(a, 7);
        const returnsB = lastDayReturn(b, 7);
        if (returnsA < returnsB) return 1;
        if (returnsA > returnsB) return -1;
        return 0;
      });
      return res.json(
        finalArray.slice(0, 10).map((item) => {
          return {
            "Fund Code": item["id"],
            "Fund Name": item["name"],
            "7 Day Return": parseFloat(item["returnValue"]).toFixed(2) + "%",
            "Latest NAV": item["nav"],
          };
        })
      );
    })
    .catch((err) => {
      return res.status(200).json({ message: "Error occured" });
    });
});

app.get("/topTenOnStandardDeviation", (req, res) => {
    getDataFromApi()
    .then((finalArray) => {
      finalArray.sort((a, b) => {
        const returnsA = standardDeviation(a);
        const returnsB = standardDeviation(b);
        if (returnsA < returnsB) return 1;
        if (returnsA > returnsB) return -1;
        return 0;
      });
      return res.json(
        finalArray.slice(0, 10).map((item) => {
          return {
            "Fund Code": item["id"],
            "Fund Name": item["name"],
            "Standard Deviation": parseFloat(item["standardDeviation"]).toFixed(2) + "%",
            "Latest NAV": item["nav"],
          };
        })
      );
    })
    .catch((err) => {
      return res.status(200).json({ message: "Error occured" });
    });
});

app.listen(port, () => {
  console.log(`Listening on port ${port}`);
});
